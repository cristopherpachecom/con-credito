package sale.test.concredito.views.clients;

import sale.test.concredito.models.clients.Clients;

public interface ClientListClickEventListener {
    void onItemClick(Clients clients);
}

