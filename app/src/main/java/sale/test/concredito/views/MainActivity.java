package sale.test.concredito.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import sale.test.concredito.R;
import sale.test.concredito.models.clients.Clients;
import sale.test.concredito.models.sales.Sales;
import sale.test.concredito.views.articles.ArticlesActivity;
import sale.test.concredito.views.clients.ClientListClickEventListener;
import sale.test.concredito.views.clients.ClientsActivity;
import sale.test.concredito.views.clients.ClientsAdapterView;
import sale.test.concredito.views.configuration.ConfigurationActivity;
import sale.test.concredito.views.sales.NoteSaleActivity;
import sale.test.concredito.views.sales.SaleListClickEventListener;
import sale.test.concredito.views.sales.SalesAdapterView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.floatingActionSale)
    FloatingActionButton floatingActionSale;

    @BindView(R.id.saleRecycleView)
    RecyclerView saleRecycleView;

    Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupDetailRecyclerView();
        realm = Realm.getDefaultInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupDetailRecyclerView() {
        saleRecycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        saleRecycleView.setLayoutManager(manager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        RealmResults<Sales> results = realm.where(Sales.class).findAll();
        ArrayList<Sales> list = new ArrayList<>(realm.copyFromRealm(results));


            SalesAdapterView adapter = new SalesAdapterView(list, new SaleListClickEventListener() {
                @Override
                public void onItemClick(Sales sales) {

                }
            });
            saleRecycleView.setAdapter(adapter);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item_client_list clicks here.
        int id = item.getItemId();

        if (id == R.id.clients) {
            showClients();
        } else if (id == R.id.acticles) {
            showArticles();
        } else if (id == R.id.config) {
            showConfiguration ();
        } else if (id == R.id.sale) {
            showSales();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showClients (){
        Intent goToClients = new Intent(this, ClientsActivity.class);
        startActivityForResult(goToClients,1);
    }

    public void showArticles (){
        Intent goToArticles = new Intent(this, ArticlesActivity.class);
        startActivityForResult(goToArticles,1);
    }

    @OnClick(R.id.floatingActionSale)
    public void showSales (){
        Intent goToSales = new Intent(this, NoteSaleActivity.class);
        startActivityForResult(goToSales,1);
    }

    public void showConfiguration (){
        Intent goToConfiguration = new Intent(this, ConfigurationActivity.class);
        startActivityForResult(goToConfiguration,1);
    }

}
