package sale.test.concredito.views.sales;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sale.test.concredito.R;
import sale.test.concredito.models.sales.Sales;


public class SalesAdapterView extends RecyclerView.Adapter<SalesAdapterView.ViewHolder> {
    private ArrayList<Sales> sales;
    private SaleListClickEventListener listener;


    public SalesAdapterView(ArrayList<Sales> sales, SaleListClickEventListener listener) {
        this.sales = sales;
        this.listener = listener;
    }
    @NonNull
    @Override
    public SalesAdapterView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_sale_list_layout, parent, false);
        return new SalesAdapterView.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesAdapterView.ViewHolder holder, int position) {
        holder.bind(sales.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return sales.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView clientName;
        private TextView folio;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            clientName = (TextView) itemView.findViewById(R.id.clientSaleItemTextView);
            folio = (TextView) itemView.findViewById(R.id.folioItemTextView);
            image = (ImageView) itemView.findViewById(R.id.modifySaleImageView);
        }

        public void bind(final Sales sale, final SaleListClickEventListener listener) {
            clientName.setText(String.valueOf(sale.getClientId())+ " - " + sale.getClient());
            folio.setText("Folio:"+sale.getFolio()+" - $"+
                    sale.getTotal()+ "  "+
                    String.valueOf(sale.getDate() ));
            image.setImageResource(R.drawable.ic_edit_black_24dp);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(sale);
                }
            });
        }
    }
}
