package sale.test.concredito.views.sales;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import sale.test.concredito.R;
import sale.test.concredito.models.articles.Articles;
import sale.test.concredito.models.clients.Clients;
import sale.test.concredito.models.sales.Sales;
import sale.test.concredito.models.sales.SalesCredit;
import sale.test.concredito.models.sales.SalesDetail;
import sale.test.concredito.utils.Constants;
import sale.test.concredito.views.MainActivity;


public class NoteSaleActivity extends AppCompatActivity {
    @BindView(R.id.addProductButtom)
    Button addProductButtom;
    @BindView(R.id.cancelSaleButton)
    Button cancelSaleButton;
    @BindView(R.id.continueButton)
    Button continueButton;

    @Nullable @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @Nullable    @BindView(R.id.threeRadioButton)
    RadioButton threeRadioButton;
    @Nullable@BindView(R.id.sixRadioButton)
    RadioButton sixRadioButton;
    @Nullable@BindView(R.id.nineRadioButton)
    RadioButton nineRadioButton;
    @Nullable@BindView(R.id.twelveRadioButton)
    RadioButton twelveRadioButton;

    @BindView(R.id.amountProductEditText)
    EditText amountProductEditText;

    @BindView(R.id.saleProductRecycleView)
    RecyclerView saleProductRecycleView;

    @BindView(R.id.folioTextView)
    TextView folioTextView;
    @BindView(R.id.depositSaleDetailTextView)
    TextView depositSaleTextView;
    @BindView(R.id.bonificationSaleDetailTextView)
    TextView bonificationSaleTextView;
    @BindView(R.id.totalSaleDetailTextView)
    TextView totalSaleTextView;


    @BindView(R.id.clientSaleAutoCompleteTextView)
    AutoCompleteTextView clientSaleAutoCompleteTextView;
    @BindView(R.id.productSaleAutoCompleteTextView)
    AutoCompleteTextView productSaleAutoCompleteTextView;

    private RadioButton radioButton;

    SharedPreferences sharedPreferences;

    String saleId;
    float deposit;
    float bonification;
    float total;
    float totalToPay;
    float totalCash;
    float importPayment;
    float importSaving;
    int folio;

    SalesCredit salesCredit;

    Realm realm;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sales_layout);
        ButterKnife.bind(this);
        setupDetailRecyclerView();
        realm = Realm.getDefaultInstance();
        saleId = UUID.randomUUID().toString();
        loadAutoComplete();
        generatefolio();
        sharedPreferences = getSharedPreferences(Constants.PREFERENCE,MODE_PRIVATE);
    }

    public void generatefolio(){
        folio = getSharedPreferences(Constants.FOLIO,MODE_PRIVATE).getInt("folio",0) + 1;
        folioTextView.setText("Folio: "+String.valueOf(folio));
    }

    public void loadAutoComplete(){
        RealmResults<Clients> clients = realm.where(Clients.class).findAll();
        RealmResults<Articles> articles = realm.where(Articles.class).findAll();
        final ArrayAdapter<String> autoCompleteClients =
                new ArrayAdapter<>(this,android.R.layout.simple_list_item_1);
        final ArrayAdapter<String> autoCompleteArticles =
                new ArrayAdapter<>(this,android.R.layout.simple_list_item_1);

        for (Clients current : clients){
            autoCompleteClients.add(
                        current.getClientId() +" - "+ current.getName() + " " +
                            current.getLastName() + " " + current.getLastSecondName());
        }
        clientSaleAutoCompleteTextView.setAdapter(autoCompleteClients);


        for (Articles current : articles){
            autoCompleteArticles.add(current.getArticleId()+" - "+current.getDescription()
                    +" - "+current.getModel());
        }
        productSaleAutoCompleteTextView.setAdapter(autoCompleteArticles);

    }

    @OnClick(R.id.addProductButtom)
    public void addProduct(){
        if (validateExistence(getId(productSaleAutoCompleteTextView.getText().toString()),
                Integer.parseInt(amountProductEditText.getText().toString()))){
            SalesDetail salesDetail = new SalesDetail();
            salesDetail.setSaleId(saleId);
            salesDetail.setProductId(getId(productSaleAutoCompleteTextView.getText().toString()));
            salesDetail.setDescription(getConcept(productSaleAutoCompleteTextView.getText().toString()));
            salesDetail.setModel(getModel(productSaleAutoCompleteTextView.getText().toString()));
            salesDetail.setPieces(Integer.parseInt(amountProductEditText.getText().toString()));

            Articles article = realm.where(Articles.class).equalTo("articleId",
                    getId(productSaleAutoCompleteTextView.getText().toString())).findFirst();


            float price = article.getPrice() *
                    (1 + (
                        sharedPreferences.getFloat(Constants.Configuration.FINANCING_RATE.getValue(),0)
                        * sharedPreferences.getInt(Constants.Configuration.DEADLINE.getValue(),0)
                    ) /100);

            salesDetail.setPrice(price);
            float importe = price * Integer.parseInt(amountProductEditText.getText().toString());
            salesDetail.setImporte(importe);
            realm.beginTransaction();
            realm.copyToRealm(salesDetail);
            realm.commitTransaction();
            updateReciclyeProducts();
            getTotals();
        }

    }

    public void getTotals(){
        RealmResults<SalesDetail> salesDetailtotal = realm.where(SalesDetail.class)
                .equalTo("saleId",saleId).findAll();


        deposit = (sharedPreferences.getFloat(Constants.Configuration.DEPOSIT_PORCENT.getValue(),0)
                /100) * salesDetailtotal.sum("importe").floatValue();

        bonification = deposit * (
                (sharedPreferences.getFloat(Constants.Configuration.FINANCING_RATE.getValue(),0)
                * sharedPreferences.getInt(Constants.Configuration.DEADLINE.getValue(),0))
                /100);

        total = salesDetailtotal.sum("importe").floatValue() - deposit - bonification;

        depositSaleTextView.setText(String.format("%.2f", deposit));
        bonificationSaleTextView.setText(String.format("%.2f", bonification));
        totalSaleTextView.setText(String.format("%.2f", total));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void deteteDetailHang(){
        final RealmResults<SalesDetail> salesDetails = realm.where(SalesDetail.class)
                .equalTo("saleId",saleId).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                salesDetails.deleteAllFromRealm();
            }
        });
    }

    public int getId(String text){
        String[] textComplete =  text.split("-");
        int id = 0 ;
        id = Integer.parseInt(textComplete[0].trim());
        return id;
    }

    public String getConcept(String text){
          String[] textComplete =  text.split("-");
          String concept = "" ;
          concept = textComplete[1].trim();
          return concept;
    }

    public String getModel(String text){
        String[] textComplete =  text.split("-");
        String concept = "" ;
        concept = textComplete[2].trim();
        return concept;
    }

    public void updateReciclyeProducts(){
        RealmResults<SalesDetail> resultDetail = realm.where(SalesDetail.class)
                .equalTo("saleId",saleId).findAll();
        ArrayList<SalesDetail> listDetail = new ArrayList<>(realm.copyFromRealm(resultDetail));


            SaleDetailAdapterView adapter = new SaleDetailAdapterView(listDetail,
                    new SaleDetailListClickEventListener() {
                @Override
                public void onItemClick(final SalesDetail salesDetail) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(NoteSaleActivity.this);
                    alert.setMessage("Seguro deseas eliminar este producto?")
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    final SalesDetail salesDetails = realm.where(SalesDetail.class)
                                            .equalTo("saleDetailId",salesDetail.getSaleDetailId())
                                            .findFirst();
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            salesDetails.deleteFromRealm();
                                            updateReciclyeProducts();
                                        }
                                    });
                                }
                            })
                            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            })
                            .create()
                            .show();

                }
            });
            saleProductRecycleView.setAdapter(adapter);

    }

    public boolean validateExistence(int productId,int quantity){
           RealmResults<SalesDetail> products = realm.where(SalesDetail.class)
                   .equalTo("productId",productId).findAll();

           Articles articles = realm.where(Articles.class).equalTo("articleId",productId).findFirst();
           int exist = articles.getExistence() - products.sum("pieces").intValue();
           if (quantity>exist){
               Toast.makeText(this,"Solo queda en existencia " + exist +" "+articles.getDescription(),Toast.LENGTH_LONG).show();
               return false;
           }
           return true;
    }

    private void setupDetailRecyclerView() {
        saleProductRecycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        saleProductRecycleView.setLayoutManager(manager);
    }

    @Override
    public void onBackPressed() {
        goToMain();
    }

    @OnClick(R.id.cancelSaleButton)
    public void goToMain(){
        deteteDetailHang();
        Intent goToMenu = new Intent(this, MainActivity.class);
        startActivityForResult(goToMenu,1);
    }

    public boolean validateText(){

        return   clientSaleAutoCompleteTextView.getText().toString().matches("")||
                 productSaleAutoCompleteTextView.getText().toString().matches("")||
                 amountProductEditText.getText().toString().matches("")||
                 totalSaleTextView.getText().toString().matches("");

    }

    @OnClick({R.id.continueButton})
    public void methodPayment(){
        if (!validateText()) {

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            final View prompt = layoutInflater.inflate(R.layout.sale_payment_layout, null);
            threeRadioButton = prompt.findViewById(R.id.threeRadioButton);
            sixRadioButton = prompt.findViewById(R.id.sixRadioButton);
            nineRadioButton = prompt.findViewById(R.id.nineRadioButton);
            twelveRadioButton = prompt.findViewById(R.id.twelveRadioButton);
            radioGroup = prompt.findViewById(R.id.radioGroup);
            setPayments();

            AlertDialog.Builder alert = new AlertDialog.Builder(NoteSaleActivity.this);
            alert.setTitle("Abonos Mensuales")
                    .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if(threeRadioButton.isChecked()){
                                salesCredit = (SalesCredit) threeRadioButton.getTag();
                            }else if (sixRadioButton.isChecked()){
                                salesCredit = (SalesCredit) sixRadioButton.getTag();
                            }else if (nineRadioButton.isChecked()){
                                salesCredit = (SalesCredit) nineRadioButton.getTag();
                            }else if (twelveRadioButton.isChecked()){
                                salesCredit = (SalesCredit) twelveRadioButton.getTag();
                            }
                            saveNote();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    })
                    .setView(prompt)
                    .create()
                    .show();

        }else{
            Toast.makeText(this,"Los datos no estan completos verifique por favor",
                    Toast.LENGTH_LONG).show();
        }
    }



    public void saveNote(){
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String date = df.format(Calendar.getInstance().getTime());

        Sales sale = new Sales();
        sale.setSaleId(saleId);
        sale.setFolio(folio);
        sale.setClient(getConcept(clientSaleAutoCompleteTextView.getText().toString()));
        sale.setClientId(getId(clientSaleAutoCompleteTextView.getText().toString()));
        sale.setDeposit(Float.parseFloat(depositSaleTextView.getText().toString()));
        sale.setBonification(Float.parseFloat(bonificationSaleTextView.getText().toString()));
        sale.setTotal(Float.parseFloat(totalSaleTextView.getText().toString()));
        sale.setSalesCredits(salesCredit);
        sale.setDate(date);

        realm.beginTransaction();
        realm.copyToRealm(sale);
        realm.commitTransaction();

        saleId="";
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.FOLIO,MODE_PRIVATE);
        sharedPreferences.edit()
                .putInt("folio",folio).apply();

        Toast.makeText(this,"Bien hecho. se guardo con exito tu venta",Toast.LENGTH_LONG).show();
        goToMain();
    }

    public void setPayments(){
        totalCash = total / (1 + (
                (sharedPreferences.getFloat(Constants.Configuration.FINANCING_RATE.getValue(),0)
                * sharedPreferences.getInt(Constants.Configuration.DEADLINE.getValue(),0))
                / 100));
        for (int i = 0; i<=sharedPreferences.getInt(Constants.Configuration.DEADLINE.getValue(),0);i++){
            if(i % 3 == 0){
                totalToPay =  totalCash  * (1 +
                        (sharedPreferences.getFloat(Constants.Configuration.FINANCING_RATE.getValue(),0)
                        * i) / 100);

                importPayment = totalToPay / i;
                importSaving = total - totalToPay;
                SalesCredit salesCredit = new SalesCredit();
                salesCredit.setTimeLimit(i);
                salesCredit.setPayment(importPayment);
                salesCredit.setTotal(totalToPay);
                salesCredit.setSaving(importSaving);
                if (i==3){
                    threeRadioButton.setTag(salesCredit);
                    threeRadioButton.setText("3 ABONOS DE "+String.format("%.2f", importPayment)+
                    "\nTOTAL A PAGAR "+String.format("%.2f", totalToPay) +
                    " \nSE AHORRA "+String.format("%.2f", importSaving));
                }else if (i==6){
                    sixRadioButton.setTag(salesCredit);
                    sixRadioButton.setText("6 ABONOS DE "+String.format("%.2f", importPayment)+
                            "\nTOTAL A PAGAR "+String.format("%.2f", totalToPay) +
                            "\nSE AHORRA "+String.format("%.2f",importSaving));
                }else if (i==9){
                    nineRadioButton.setTag(salesCredit);
                    nineRadioButton.setText("9 ABONOS DE "+String.format("%.2f", importPayment)+
                            "\nTOTAL A PAGAR "+String.format("%.2f", totalToPay) +
                            "\nSE AHORRA "+String.format("%.2f", importSaving));
                }else if(i==12) {
                    twelveRadioButton.setTag(salesCredit);
                    twelveRadioButton.setText("12 ABONOS DE "+String.format("%.2f",importPayment)+
                            "\nTOTAL A PAGAR "+String.format("%.2f",totalToPay) +
                            "\nSE AHORRA "+String.format("%.2f",importSaving));
                }
            }
        }

    }

}

