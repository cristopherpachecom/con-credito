package sale.test.concredito.views.sales;

import sale.test.concredito.models.sales.SalesDetail;

public interface SaleDetailListClickEventListener {
    void onItemClick(SalesDetail salesDetail);
}
