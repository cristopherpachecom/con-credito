package sale.test.concredito.views.clients;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sale.test.concredito.R;
import sale.test.concredito.models.clients.Clients;

public class ClientsAdapterView extends RecyclerView.Adapter<ClientsAdapterView.ViewHolder> {
    private ArrayList<Clients> clients;
    private ClientListClickEventListener listener;

    public ClientsAdapterView(ArrayList<Clients> clients, ClientListClickEventListener listener) {
        this.clients = clients;
        this.listener = listener;
    }

    // Crear la vista que se va a mostrar
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_client_list, parent, false);
        return new ClientsAdapterView.ViewHolder(item);
    }



    // El bindeo de los datos con la vista
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(clients.get(position), listener);
    }
    // El tamaño de los elementos
    @Override
    public int getItemCount() {
        return clients.size();
    }
    // Clase que permite crear la vista del item en el recycler
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView clientName;
        private TextView clave;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            clientName = (TextView) itemView.findViewById(R.id.clientNameItemTextView);
            clave = (TextView) itemView.findViewById(R.id.claveItemTextView);
            image = (ImageView) itemView.findViewById(R.id.modifyImageView);
        }

        public void bind(final Clients client, final ClientListClickEventListener listener) {
            clientName.setText(client.getName()+ " " + client.getLastName() + " " + client.getLastSecondName() );
            clave.setText("Clave: " + String.valueOf(client.getClientId()));
            image.setImageResource(R.drawable.ic_edit_black_24dp);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(client);
                }
            });
        }
    }
}