package sale.test.concredito.views.configuration;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sale.test.concredito.R;
import sale.test.concredito.utils.Constants;
import sale.test.concredito.views.MainActivity;


public class ConfigurationActivity extends AppCompatActivity {

    @BindView(R.id.financingEditText)
    EditText financingEditText;
    @BindView(R.id.depositEditText)
    EditText depositEditText;
    @BindView(R.id.deadLineEditText)
    EditText deadLineEditText;

    @BindView(R.id.financingTextView)
    TextView financingTextView;
    @BindView(R.id.depositTextView)
    TextView depositTextView;
    @BindView(R.id.deadLineTextView)
    TextView deadLineTextView;

    @BindView(R.id.saveButton)
    Button saveButton;
    @BindView(R.id.cancelButton)
    Button cancelButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configuration_layout);


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        String fecha = dateFormat.format(date);
        getSupportActionBar().setTitle("Fecha: " + fecha);


        ButterKnife.bind(this);
        loadConfiguration();
    }

    @OnClick
    (R.id.saveButton)
    public void saveConfiguration(){
        if (validateConfiguration()){
            SharedPreferences sharedPreferences =
                    getSharedPreferences(Constants.PREFERENCE,MODE_PRIVATE);
            sharedPreferences.edit()
                    .putFloat(Constants.Configuration.FINANCING_RATE.getValue(),
                            Float.parseFloat(financingEditText.getText().toString()) )
                    .putFloat(Constants.Configuration.DEPOSIT_PORCENT.getValue(),
                            Float.parseFloat(depositEditText.getText().toString()) )
                    .putInt(Constants.Configuration.DEADLINE.getValue(),
                            Integer.parseInt(deadLineEditText.getText().toString()) )
                    .apply();

            Toast.makeText(this,
                    "Bien Hecho. La configuración ha sido registrada",
                    Toast.LENGTH_LONG)
                    .show();

            goToMainMenu();
        }
    }

    @OnClick
    (R.id.cancelButton)
    public void cancelButton () {
        AlertDialog.Builder  alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage("¿Seguro que desea salir de la pantalla actual?")
        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                goToMainMenu();
            }
        })
        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        })
        .create()
        .show();
    }

    public void loadConfiguration (){
        SharedPreferences sharedPreferences =
                getSharedPreferences(Constants.PREFERENCE,MODE_PRIVATE);

            financingEditText.setHint(String.valueOf(sharedPreferences.getFloat
                    (Constants.Configuration.FINANCING_RATE.getValue(), 0) ));
            depositEditText.setHint(String.valueOf( sharedPreferences.getFloat
                    (Constants.Configuration.DEPOSIT_PORCENT.getValue(), 0) ));
            deadLineEditText.setHint(String.valueOf( sharedPreferences.getInt
                    (Constants.Configuration.DEADLINE.getValue(), 0) ));

    }

    public boolean validateConfiguration(){
        if (deadLineEditText.getText().toString().matches("")  ||
                depositEditText.getText().toString().matches("") ||
                financingEditText.getText().toString().matches("")) {

            Toast.makeText(this,"No guades guardar una configuracion vacia.",
                    Toast.LENGTH_LONG).show();

            return false;
        }else{
            return true;
        }
    }

    public void goToMainMenu(){
        Intent goToMainMenu = new Intent(this, MainActivity.class);
        startActivityForResult(goToMainMenu,1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
