package sale.test.concredito.views.articles;

import sale.test.concredito.models.articles.Articles;

public interface ArticlesListClickEventListener {
    void onItemClick(Articles articles);
}
