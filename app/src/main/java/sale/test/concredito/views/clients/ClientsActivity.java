package sale.test.concredito.views.clients;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import sale.test.concredito.R;
import sale.test.concredito.models.clients.Clients;
import sale.test.concredito.views.MainActivity;

public class ClientsActivity extends AppCompatActivity {
    @Nullable @BindView (R.id.claveTextView)
    TextView claveTextView;
    @Nullable@BindView(R.id.nameTextView)
    TextView nameTextView;

    @Nullable@BindView(R.id.lastNameTextView)
    TextView lastNameTextView;
    @Nullable@BindView(R.id.lastSecondNameTextView)
    TextView lastSecondNameTextView;
    @Nullable@BindView(R.id.rfcTextView)
    TextView rfcTextView;

    @Nullable@BindView(R.id.nameEditText)
    EditText nameEditText;
    @Nullable@BindView(R.id.lastNameEditText)
    EditText lastNameEditText;
    @Nullable@BindView(R.id.lastSecondNameEditText)
    EditText lastSecondNameEditText;
    @Nullable@BindView(R.id.rfcEditText)
    EditText rfcEditText;

    @Nullable @BindView(R.id.saveClientButton)
    Button saveClientButton;
    @Nullable @BindView(R.id.cancelClientButton)
    Button cancelClientButton;

    @BindView(R.id.floatingActionClient)
    FloatingActionButton floatingActionClient;

    @BindView(R.id.clientsRecycleView)
    RecyclerView clientsRecycleView;
    
    int clientId = 0;

    Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_clients_layout);
        ButterKnife.bind(this);
        setupClientsRecyclerView();
        realm = Realm.getDefaultInstance();
    }

    private void setupClientsRecyclerView() {
        clientsRecycleView.setHasFixedSize(true);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        clientsRecycleView.setLayoutManager(manager);
    }
    
    @Override
    protected void onResume() {
        super.onResume();

        RealmResults<Clients> resultsClients = realm.where(Clients.class).findAll();
        ArrayList<Clients> listClients = new ArrayList<>(realm.copyFromRealm(resultsClients));

                if (resultsClients.size()>0 ) {
                    ClientsAdapterView adapter = new ClientsAdapterView(listClients, new ClientListClickEventListener() {
                        @Override
                        public void onItemClick(Clients clients) {
                            clientId = clients.getClientId();
                            addClient();
                        }
                    });
                    clientsRecycleView.setAdapter(adapter);
                    
                }
    }


    @Override
    public void onBackPressed() {
        goToMenu();
    }

    @OnClick(R.id.floatingActionClient)
    public void addClient () {
        RealmResults<Clients> clients = realm.where(Clients.class).findAll();
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View prompt = layoutInflater.inflate(R.layout.clients_layout, null);
        claveTextView = prompt.findViewById(R.id.claveTextView);

        nameEditText = prompt.findViewById(R.id.nameEditText);
        lastNameEditText = prompt.findViewById(R.id.lastNameEditText);
        lastSecondNameEditText = prompt.findViewById(R.id.lastSecondNameEditText);
        rfcEditText = prompt.findViewById(R.id.rfcEditText);
        if (clients.size() > 0){
            if (clientId == 0 ) {
                clientId = clients.where().max("clientId").intValue() + 1;
                claveTextView.setText("Clave: " + String.valueOf( clientId ));
            }else {
                Clients client = realm.where(Clients.class).equalTo("clientId",clientId).findFirst();
                claveTextView.setText("Clave: " + String.valueOf(client.getClientId()));
                nameEditText.setText(client.getName());
                lastNameEditText.setText(client.getLastName());
                lastSecondNameEditText.setText(client.getLastSecondName());
                rfcEditText.setText(client.getRfc());
            }
        }else {
            clientId = 1;
            claveTextView.setText("Clave: 1");
        }


        saveClientButton = prompt.findViewById(R.id.saveClientButton);
        saveClientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = null;
                if (nameEditText.getText().toString().matches("") ||
                        lastNameEditText.getText().toString().matches("") ||
                        lastSecondNameEditText.getText().toString().matches("") ||
                        rfcEditText.getText().toString().matches(""))
                {
                    if (nameEditText.getText().toString().matches("") ){
                        message = "nombre";
                    }else if (lastNameEditText.getText().toString().matches("")){
                        message = "apellido paterno";
                    }else if (lastSecondNameEditText.getText().toString().matches("")){
                        message = "apellido materno";
                    }else if (rfcEditText.getText().toString().matches("")){
                        message = "RFC";
                    }

                    Snackbar.make(view, "El campo " + message + "esta vacio, favor de llenarlo.",
                            Snackbar.LENGTH_LONG).show();

                }else{
                    Clients saveClient = new Clients();
                    saveClient.setClientId(clientId);
                    saveClient.setName(nameEditText.getText().toString());
                    saveClient.setLastName(lastNameEditText.getText().toString());
                    saveClient.setLastSecondName(lastSecondNameEditText.getText().toString());
                    saveClient.setRfc(rfcEditText.getText().toString());
                    realm.beginTransaction();
                    realm.insertOrUpdate(saveClient);
                    realm.commitTransaction();
                    realm.close();

                    nameEditText.setText("");
                    lastNameEditText.setText("");
                    lastSecondNameEditText.setText("");
                    rfcEditText.setText("");

                    Snackbar.make(view, "Bien hecho. El cliente a sido registrado exitosamente",
                            Snackbar.LENGTH_LONG).show();

                    goToClients();
                }

            }
        });
        cancelClientButton = prompt.findViewById(R.id.cancelClientButton);
        cancelClientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "¿Seguro que desea salir de la pantalla?", Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(R.color.colorAccent))
                        .setAction("Si", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                goToClients();
                            }
                        })
                        .show();
            }
        });


        AlertDialog.Builder alertClient = new AlertDialog.Builder(this);
        alertClient .setTitle("Cliente")
                    .setView(prompt)
                    .create()
                    .show();
    }

    public void goToClients(){
        Intent goToClients = new Intent(this, ClientsActivity.class);
        startActivityForResult(goToClients,1);
    }

    public void goToMenu(){
        Intent goToMenu = new Intent(this, MainActivity.class);
        startActivityForResult(goToMenu,1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

}
