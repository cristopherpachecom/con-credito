package sale.test.concredito.views.sales;

import sale.test.concredito.models.sales.Sales;

public interface SaleListClickEventListener {
    void onItemClick(Sales sale);
}
