package sale.test.concredito.views.sales;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sale.test.concredito.R;
import sale.test.concredito.models.sales.Sales;
import sale.test.concredito.models.sales.SalesDetail;
import sale.test.concredito.views.clients.ClientsAdapterView;


public class SaleDetailAdapterView extends RecyclerView.Adapter<SaleDetailAdapterView.ViewHolder> {
    private ArrayList<SalesDetail> salesDetails;
    private SaleDetailListClickEventListener listener;


    public SaleDetailAdapterView(ArrayList<SalesDetail> salesDetail, SaleDetailListClickEventListener listener) {
        this.salesDetails = salesDetail;
        this.listener = listener;
    }


    @Override
    public SaleDetailAdapterView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_sale_list_layout, parent, false);
        return new SaleDetailAdapterView.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull SaleDetailAdapterView.ViewHolder holder, int position) {
        holder.bind(salesDetails.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return salesDetails.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView description;
        private TextView prices;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            description = (TextView) itemView.findViewById(R.id.clientSaleItemTextView);
            prices = (TextView) itemView.findViewById(R.id.folioItemTextView);
            image = (ImageView) itemView.findViewById(R.id.modifySaleImageView);
        }

        public void bind(final SalesDetail salesDetail, final SaleDetailListClickEventListener listener) {
            description.setText(String.valueOf(salesDetail.getPieces())+ " - " +
                    salesDetail.getDescription() + "(" + salesDetail.getModel() + ") ");
            prices.setText("Precio: $"+String.format("%.2f",salesDetail.getPrice())+" - Importe: $"+ String.format("%.2f",salesDetail.getImporte()));
            image.setImageResource(R.drawable.ic_delete_forever_black_24dp);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(salesDetail);
                }
            });
        }
    }

}