package sale.test.concredito.views.articles;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sale.test.concredito.R;
import sale.test.concredito.models.articles.Articles;


public class ArticlesAdapterView extends RecyclerView.Adapter<ArticlesAdapterView.ViewHolder> {
    private ArrayList<Articles> articles;
    private ArticlesListClickEventListener listener;

    public ArticlesAdapterView(ArrayList<Articles> articles, ArticlesListClickEventListener listener) {
        this.articles = articles;
        this.listener = listener;
    }


    @Override
    public ArticlesAdapterView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_articles_list_layout, parent, false);
        return new ArticlesAdapterView.ViewHolder(item);
    }


    @Override
    public void onBindViewHolder(ArticlesAdapterView.ViewHolder holder, int position) {
        holder.bind(articles.get(position), listener);
    }


    @Override
    public int getItemCount() {
        return articles.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView description;
        private TextView clave;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            description = (TextView) itemView.findViewById(R.id.descriptionNameItemTextView);
            clave = (TextView) itemView.findViewById(R.id.claveArticleItemTextView);
            image = (ImageView) itemView.findViewById(R.id.modifyArticleImageView);
        }

        public void bind(final Articles articles, final ArticlesListClickEventListener listener) {
            description.setText(articles.getDescription());
            clave.setText("Clave: " + String.valueOf(articles.getArticleId()));
            image.setImageResource(R.drawable.ic_edit_black_24dp);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(articles);
                }
            });
        }
    }
}
