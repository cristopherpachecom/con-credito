package sale.test.concredito.views.articles;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import sale.test.concredito.R;
import sale.test.concredito.models.articles.Articles;
import sale.test.concredito.views.MainActivity;


public class ArticlesActivity extends AppCompatActivity {
    @Nullable
    @BindView(R.id.claveArticleTextView)
    TextView claveArticleTextView;
    @Nullable@BindView(R.id.descriptionTextView)
    TextView descriptionTextView;

    @Nullable@BindView(R.id.modelTextView)
    TextView modelTextView;
    @Nullable@BindView(R.id.priceTextView)
    TextView priceTextView;
    @Nullable@BindView(R.id.existTextView)
    TextView existTextView;

    @Nullable@BindView(R.id.descriptionEditText)
    EditText descriptionEditText;
    @Nullable@BindView(R.id.modelEditText)
    EditText modelEditText;
    @Nullable@BindView(R.id.priceEditText)
    EditText priceEditText;
    @Nullable@BindView(R.id.existEditText)
    EditText existEditText;

    @Nullable @BindView(R.id.saveArticlesButton)
    Button saveArticlesButton;
    @Nullable @BindView(R.id.cancelArticlesButton)
    Button cancelArticlesButton;

    @BindView(R.id.floatingActionArticles)
    FloatingActionButton floatingActionArticles;

    @BindView(R.id.articlesRecycleView)
    RecyclerView articlesRecycleView;

    int articleId = 0;

    Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_articles_layout);
        ButterKnife.bind(this);
        setupClientsRecyclerView();
        realm = Realm.getDefaultInstance();
    }

    private void setupClientsRecyclerView() {
        articlesRecycleView.setHasFixedSize(true);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        articlesRecycleView.setLayoutManager(manager);
    }

    @Override
    protected void onResume() {
        super.onResume();

        RealmResults<Articles> resultsArticles = realm.where(Articles.class).findAll();
        ArrayList<Articles> listArticles = new ArrayList<>(realm.copyFromRealm(resultsArticles));

        if (resultsArticles.size()>0 ) {
            ArticlesAdapterView adapter = new ArticlesAdapterView(
                    listArticles, new ArticlesListClickEventListener() {
                @Override
                public void onItemClick(Articles articles) {
                    articleId = articles.getArticleId();
                    addArticle();
                }
            });
            articlesRecycleView.setAdapter(adapter);

        }
    }


    @Override
    public void onBackPressed() {
        goToMenu();
    }

    @OnClick(R.id.floatingActionArticles)
    public void addArticle () {
        RealmResults<Articles> articles = realm.where(Articles.class).findAll();
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View prompt = layoutInflater.inflate(R.layout.articles_layout, null);
        claveArticleTextView = prompt.findViewById(R.id.claveArticleTextView);

        descriptionEditText = prompt.findViewById(R.id.descriptionEditText);
        modelEditText = prompt.findViewById(R.id.modelEditText);
        priceEditText = prompt.findViewById(R.id.priceEditText);
        existEditText = prompt.findViewById(R.id.existEditText);
        if (articles.size() > 0){
            if (articleId == 0 ) {
                articleId = articles.where().max("articleId").intValue() + 1;
                claveArticleTextView.setText("Clave: " + String.valueOf( articleId ));
            }else {
                Articles article = realm.where(Articles.class)
                        .equalTo("articleId",articleId).findFirst();
                claveArticleTextView.setText("Clave: " + String.valueOf(article.getArticleId()));
                descriptionEditText.setText(article.getDescription());
                modelEditText.setText(article.getModel());
                priceEditText.setText(String.valueOf(article.getPrice() ));
                existEditText.setText(String.valueOf( article.getExistence() ));
            }
        }else {
            articleId = 1;
            claveArticleTextView.setText("Clave: 1");
        }


        saveArticlesButton = prompt.findViewById(R.id.saveArticlesButton);
        saveArticlesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = null;
                if (descriptionEditText.getText().toString().matches("") ||
                        modelEditText.getText().toString().matches("") ||
                        priceEditText.getText().toString().matches("") ||
                        existEditText.getText().toString().matches(""))
                {
                    if (descriptionEditText.getText().toString().matches("") ){
                        message = "descripcion";
                    }else if (modelEditText.getText().toString().matches("")){
                        message = "modelo";
                    }else if (priceEditText.getText().toString().matches("")){
                        message = "precio";
                    }else if (existEditText.getText().toString().matches("")){
                        message = "existencia";
                    }

                    Snackbar.make(view, "El campo "+message+ "esta vacio, favor de llenarlo.",
                            Snackbar.LENGTH_LONG).show();

                }else{
                    Articles saveArticle = new Articles();
                    saveArticle.setArticleId(articleId);
                    saveArticle.setDescription(descriptionEditText.getText().toString());
                    saveArticle.setModel(modelEditText.getText().toString());
                    saveArticle.setPrice(Float.parseFloat(priceEditText.getText().toString()));
                    saveArticle.setExistence(Integer.parseInt(existEditText.getText().toString()));

                    realm.beginTransaction();
                    realm.insertOrUpdate(saveArticle);
                    realm.commitTransaction();
                    realm.close();

                    descriptionEditText.setText("");
                    modelEditText.setText("");
                    priceEditText.setText("");
                    existEditText.setText("");

                    Snackbar.make(view, "Bien hecho. El articulo a sido registrado exitosamente",
                            Snackbar.LENGTH_LONG).show();

                    goToArticles();
                }

            }
        });
        cancelArticlesButton = prompt.findViewById(R.id.cancelArticlesButton);
        cancelArticlesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "¿Seguro que desea salir de la pantalla?",
                        Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(R.color.colorAccent))
                        .setAction("Si", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                goToArticles();
                            }
                        })
                        .show();
            }
        });


        AlertDialog.Builder alertArticles = new AlertDialog.Builder(this);
        alertArticles .setTitle("Articulos")
                .setView(prompt)
                .create()
                .show();
    }

    public void goToArticles(){
        Intent goToArticles = new Intent(this, ArticlesActivity.class);
        startActivityForResult(goToArticles,1);
    }

    public void goToMenu(){
        Intent goToMenu = new Intent(this, MainActivity.class);
        startActivityForResult(goToMenu,1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

}
