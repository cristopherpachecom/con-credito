package sale.test.concredito.utils;

public class Constants {

    public static String PREFERENCE = "configuration";
    public static String FOLIO = "folio";

    public enum Configuration {
        FINANCING_RATE("financingRate"),
        DEPOSIT_PORCENT("depositPorcent"),
        DEADLINE("deadline");

        private String value;

        private Configuration(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

}
