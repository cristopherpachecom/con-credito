package sale.test.concredito.models.sales;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Sales extends RealmObject {
    @PrimaryKey
    private String saleId;
    private int folio;
    private int clientId;
    private String client;
    private String rfc;
    private String date;
    private float deposit;
    private float bonification;
    private float total;
    private RealmList<SalesDetail> salesDetails;
    private SalesCredit salesCredits;

    public Sales() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public RealmList<SalesDetail> getSalesDetails() {
        return salesDetails;
    }

    public void setSalesDetails(RealmList<SalesDetail> salesDetails) {
        this.salesDetails = salesDetails;
    }

    public SalesCredit getSalesCredits() {
        return salesCredits;
    }

    public void setSalesCredits(SalesCredit salesCredits) {
        this.salesCredits = salesCredits;
    }

    public float getDeposit() {
        return deposit;
    }

    public void setDeposit(float deposit) {
        this.deposit = deposit;
    }

    public float getBonification() {
        return bonification;
    }

    public void setBonification(float bonification) {
        this.bonification = bonification;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
