package sale.test.concredito.models.articles;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Articles extends RealmObject {
    @PrimaryKey
    private int articleId;
    private String description;
    private String model;
    private float price;
    private int existence;

    public Articles() {
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getExistence() {
        return existence;
    }

    public void setExistence(int existence) {
        this.existence = existence;
    }
}
