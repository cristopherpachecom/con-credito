package sale.test.concredito.models.clients;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Clients extends RealmObject {
    @PrimaryKey
    private int clientId;
    private String name;
    private String lastName;
    private String lastSecondName;
    private String rfc;

    public Clients() {
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastSecondName() {
        return lastSecondName;
    }

    public void setLastSecondName(String lastSecondName) {
        this.lastSecondName = lastSecondName;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
}
