package sale.test.concredito.models.sales;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SalesCredit extends RealmObject {
    @PrimaryKey
    private String saleCredit = UUID.randomUUID().toString();
    private int timeLimit;
    private float payment;
    private float total;
    private float saving;

    public SalesCredit() {
    }

    public String getSaleCredit() {
        return saleCredit;
    }

    public void setSaleCredit(String saleCredit) {
        this.saleCredit = saleCredit;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public float getPayment() {
        return payment;
    }

    public void setPayment(float payment) {
        this.payment = payment;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getSaving() {
        return saving;
    }

    public void setSaving(float saving) {
        this.saving = saving;
    }
}
